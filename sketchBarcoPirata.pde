Scenario s;
ControlZone cz;

void setup() {
  size(800, 600);
  
  // create Scenario
  s = new Scenario(new PVector(0, 0), width, height);
  
  // create Control Zone    
  int cz_height = height/4;
  int cz_width = width/4;  
  PVector cz_pos = new PVector(width/2-cz_width/2, height-cz_height);
  cz = new ControlZone(cz_pos, cz_width, cz_height);
  
  // characters
  BarcoPirata barcoPirata = new BarcoPirata(width/2, height/2, 140);
  
  s.addCharacter(barcoPirata);
  cz.assignCharacter(barcoPirata);
}

void draw() {
  s.display();  
  cz.display();
}