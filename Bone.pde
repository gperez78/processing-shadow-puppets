class Bone {
  
  Node myNode;
  PImage imgBone;
  float dim;

  Bone(PImage img, int xpos, int ypos, int dimen) {   
    dim = dimen;
    imgBone = img;
    myNode = new Node(xpos, ypos);
  }
  
  Node getNodes() {
     // TODO: en este caso devuelve uno, 
     // pero siempre devolveriamos un array para estandarizar 
     return this.myNode; 
  }
  
  void display() {
    
    // al tener un solo nodo, la posicion del hueso es la posicion del nodo
    translate(myNode.x, myNode.y);
   
    image(imgBone,0,0, dim, dim);
  }  
}