class BarcoPirata implements Character {
  
  Bone myBone; // este personaje tiene un solo "hueso"
  PImage img;
  
  BarcoPirata(int x, int y, int dim) {
      img = loadImage("black-pearl-hi.png");      
      myBone = new Bone(img, x, y, dim);
  }
  
  Node getNodes() {
    return myBone.getNodes(); 
  }
  
  void display() {
    pushMatrix();      
      myBone.display();
    popMatrix();
  }
}

interface Character {
  
  Node getNodes(); // TODO: es un futuro, en vez de Node, seria un array Node[]
  void display();
}