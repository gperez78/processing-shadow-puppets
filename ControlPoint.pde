class MouseControlPoint implements ControlPoint {
    
    Node node;
    PVector pos;
    ControlZone cz;
    color c;
    
    MouseControlPoint(ControlZone cz, Node node) {
      
        // el area donde existe este punto de control
        this.cz = cz;
      
        // se le asigna un nodo para que controle
        this.node = node;
        
        pos = new PVector();
        c = color(240, 20, 20);
    }
        
    // mueve un nodo de un personaje al mover el mouse sobre la zona de control.   
    void update() {
            
        // mapeo de zona de control a pantalla
        pos.x = map(mouseX, cz.pos.x, cz.pos.x+cz.w, 0, width);
        pos.y = map(mouseY, cz.pos.y, cz.pos.y+cz.h, 0, height);
        
        this.node.setPosition(pos.x, pos.y); 
        
        this.display();
    }
    
    void display() {         
         fill(c);
         stroke(c);
         ellipse(mouseX, mouseY, 20, 20); 
    }
}

interface ControlPoint {
  void update(); 
  void display();
}