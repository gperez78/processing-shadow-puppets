abstract class Panel {
  
   PVector pos;
   int w, h;
      
   Panel(PVector pos, int w, int h) {
    this.pos = pos;
    this.w = w;
    this.h = h;
   }
  
   void display() {}
}