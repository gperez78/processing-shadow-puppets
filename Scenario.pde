PImage bg;

class Scenario extends Panel {
  
   ArrayList<Character> characters;
  
   Scenario(PVector pos, int w, int h) {
     super(pos, w, h);
     bg = loadImage("bg.jpg");
     characters = new ArrayList<Character>();
   }
   
   void addCharacter(Character ch) {     
     characters.add(ch);
   }
   
   void display() {
     background(bg); // TODO: dibujar en los limites del escenario, no en pantalla completa 
    
     for (Character ch : characters) {
        ch.display(); 
     }
   }
}