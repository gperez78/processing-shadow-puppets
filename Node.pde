class Node {
  float x, y; 
  
  Node(float x, float y) {
    this.setPosition(x, y);
  }
  
  // desplazar nodo
  void setPosition(float x, float y) {    
    this.x = x;
    this.y = y;
  }  
}