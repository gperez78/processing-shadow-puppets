ControlPoint cp;

class ControlZone extends Panel {
  
   Character ch;
   
   ControlZone(PVector pos, int w, int h) {
     super(pos, w, h);        
   }
  
   void assignCharacter(Character ch) {
    
    this.ch = ch;
    
    // Asignación del nodos a puntos de control
    Node nodo = ch.getNodes(); 
  
    cp = new MouseControlPoint(this, nodo);
   }
  
   void display() {    
     
      pushMatrix(); 
      translate(pos.x, pos.y);
      
      // dibujar panel
      fill(30);
      noStroke();
      rect(0, 0, w, h);   
      
      popMatrix();
      
      // dibujar punto de control si el mouse esta por encima
      if(hover()) {
        cp.update();
      }
   }
     
   boolean hover() {
      return (mouseX > pos.x && mouseX < pos.x + w 
              && mouseY > pos.y && mouseY < pos.y + h) ? true : false;
   }
}